# :coding: utf-8
# :copyright: Copyright (c) 2019 ftrack
import logging
import credentials

from textblob import TextBlob
import ftrack_api
from ftrack_action_handler.action import BaseAction

class NotesAction(BaseAction):
    '''Custom action.'''

    label = 'Calculate approval'
    identifier = 'my.hapiness.action'
    description = 'Make sentiment analysis'
    happiness_index= 0

    def discover(self, session, entities, event):
        if not self.validate_selection(entities):
            return super(NotesAction, self).discover(
                session, entities,  event
            )

        return True

    def validate_selection(self, entities):
        '''Return True if *entities* is valid'''
        return True

    def launch(self, session, entities, event):
        '''Callback method for action.'''
        self.logger.info(
            u'Launching action with selection {0}'.format(entities)
        )

        # Validate selection and abort if not valid
        if not self.validate_selection(entities):
            self.logger.warning(
                'Selection is not valid, aborting action'
            )

            return

        notes = self.detect_notes(session, entities)
        happiness_index = self.count_happiness_index(notes)

        return {
            'success': True,
            'message': 'This item has a happiness index of "{0}%"'.format(happiness_index)
        }


    def detect_notes(self, session, entities):
        notes = []
        for item in entities:
            _id = item[1]
            notes = session.query("Note where parent_id is {}".format(_id)).all()
            if notes:
                notes.extend(notes)
        
        return notes

    def count_happiness_index(self, notes):
        ''' Return average polarity for all notes calculated by TextBlob'''
        # documentation https://textblob.readthedocs.io
        count = 0
        number_of_comments = len(notes)
        if notes:
            for item in notes:
                testimonial = TextBlob(item['content'])
                count += testimonial.sentiment.polarity
                print(count)

        if not count:
            return 0
        return round(((count / number_of_comments) * 100), 2)


def register(session, **kw):
    '''Register plugin.'''

    # Validate that session is an instance of ftrack_api.Session. If not,
    # assume that register is being called from an incompatible API
    # and return without doing anything.
    if not isinstance(session, ftrack_api.Session):
        # Exit to avoid registering this plugin again.
        return

    action = NotesAction(session)
    action.register()


if __name__ == '__main__':
        
    logging.basicConfig(level=logging.INFO)
    session = ftrack_api.Session(
        server_url=credentials.FTRACK_SERVER_URL,
        api_key=credentials.FTRACK_API_KEY,
        api_user=credentials.FTRACK_API_USER,
    )
    register(session)

    # Wait for events.
    session.event_hub.wait()
